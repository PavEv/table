import React from 'react';

import './Content.css';

export const Content = ({ children }) => <div className="content">{children}</div>;
