import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import * as serviceWorker from './serviceWorker';

import rootReducer from './reducers';
import { MainPage } from './containers';
import { Container, Wrapper } from './components';

const store = createStore(rootReducer(), composeWithDevTools());

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Container>
          <Wrapper>
            <MainPage />
          </Wrapper>
        </Container>
      </Provider>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
