import { months } from '../constants';

export const dateUtils = {
  getDateByString: (dateString) => {
    const matchDay = new RegExp(/\d{1,2}/);
    const matchMonth = new RegExp(/\d{1,2}/);
    const matchYear = new RegExp(/\d{1,4}$/);
    const matchChars = new RegExp(/([\s.,\\/-])/g);

    let date = dateString.substr(0);

    const day = date.match(matchDay)[0];
    date = date.replace(matchDay, '');

    const year = date.match(matchYear)[0];
    date = date.replace(matchYear, '');

    date = date.replace(matchChars, '');
    const month = parseInt(date.search(matchMonth) === 0 ? date.match(matchMonth)[0] : months.indexOf(date))
      - 1;
    return new Date(year, month, day);
  },
};
