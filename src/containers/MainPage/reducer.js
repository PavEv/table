import { handleActions } from 'redux-actions';
import {
  setSelectedField,
  setSelectedSortType,
  setSortedData,
  setSearchInputValue,
} from './actions';
import { emptySelectOption, sortSelectOptions, defaultData } from '../../constants';

const initialState = {
  data: defaultData,
  sortedData: defaultData,
  selectedField: emptySelectOption,
  selectedSortType: sortSelectOptions[0],
  searchInputValue: '',
};

const mainPage = handleActions(
  {
    [setSelectedField](state, { payload }) {
      return { ...state, selectedField: payload.field };
    },
    [setSelectedSortType](state, { payload }) {
      return { ...state, selectedSortType: payload.sortType };
    },
    [setSortedData](state, { payload }) {
      return { ...state, sortedData: payload.data };
    },
    [setSearchInputValue](state, { payload }) {
      return { ...state, searchInputValue: payload.value };
    },
  },
  initialState
);

export default mainPage;
