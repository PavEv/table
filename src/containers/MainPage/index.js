import { connect } from 'react-redux';
import MainPageComponent from './MainPage';
import {
  setSelectedField,
  setSelectedSortType,
  setSortedData,
  setSearchInputValue,
} from './actions';

const mapStateToProps = state => ({
  data: state.mainPage.data,
  selectedField: state.mainPage.selectedField,
  selectedSortType: state.mainPage.selectedSortType,
  sortedData: state.mainPage.sortedData,
  searchInputValue: state.mainPage.searchInputValue,
});

const mapDispatchToProps = {
  setSelectedField,
  setSelectedSortType,
  setSortedData,
  setSearchInputValue,
};

export const MainPage = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPageComponent);
