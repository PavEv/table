import React from 'react';

import './Table.css';

export const Table = ({ children, headerCellItems }) => (
  <div className="table">
    <div className="table-head">
      {headerCellItems.map(headerCell => (
        <div key={headerCell.value} className="table-head__item">
          {headerCell.label}
        </div>
      ))}
    </div>
    <div className="table-body">{children}</div>
  </div>
);
