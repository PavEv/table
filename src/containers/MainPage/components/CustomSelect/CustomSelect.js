import React from 'react';
import Select from 'react-select';

import './CustomSelect.css';
import { emptySelectOption } from '../../../../constants';

export const CustomSelect = ({
  value, options, onOptionClick, isDisabled, isEmptyOption,
}) => {
  const selectOptions = isEmptyOption ? [emptySelectOption, ...options] : options;
  return (
    <Select
      className="select"
      isSearchable={false}
      value={value}
      options={selectOptions}
      onChange={onOptionClick}
      isDisabled={isDisabled}
    />
  );
};
