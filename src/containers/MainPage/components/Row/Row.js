import React from 'react';

import './Row.css';

export const Row = ({ item, activeFieldKey }) => (
  <div className="table-body-item">
    {Object.keys(item)
      .filter(itemKey => itemKey !== 'id')
      .map(itemKey => (
        <div
          key={itemKey}
          className={`table-body-item__field ${
            activeFieldKey === itemKey ? 'table-body-item__field--active' : ''
          }`}
        >
          {item[itemKey]}
        </div>
      ))}
  </div>
);
