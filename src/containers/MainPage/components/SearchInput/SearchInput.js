import React from 'react';

import './SearchInput.css';

export const SearchInput = ({ value, onChangeText, disabled }) => (
  <div className="search">
    <input
      className="search__input"
      placeholder="Поиск..."
      onChange={onChangeText}
      disabled={disabled}
      value={value}
    />
  </div>
);
