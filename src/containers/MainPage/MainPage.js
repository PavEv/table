import React, { Component } from 'react';
import _ from 'lodash';
import { Content } from '../../components';
import {
  Table, Row, CustomSelect, SearchInput,
} from './components';
import { dataObjectKeys, sortSelectOptions, descSortKey } from '../../constants';
import { dateUtils } from '../../utils';

class MainPageComponent extends Component {
  onFieldOptionClick = (value) => {
    this.props.setSelectedField({ field: value });
    this.props.setSelectedSortType({ sortType: sortSelectOptions[0] });
    this.props.setSortedData({ data: this.props.data });
  };

  onSortTypeOptionClick = (option) => {
    this.props.setSelectedSortType({ sortType: option });
    if (!option.value) {
      this.props.setSortedData({ data: this.props.data });
      return;
    }

    let sortedData = [];
    if (this.props.selectedField.value === 'date') {
      const sortedIdsByDate = this.getSortedIdsByDate();
      sortedData = sortedIdsByDate.map(id => _.find(this.props.data, item => item.id === id));
    } else {
      sortedData = _.sortBy(this.props.data, this.props.selectedField.value);
    }

    if (option.value === descSortKey) {
      sortedData = _.reverse(sortedData);
    }

    this.props.setSortedData({ data: sortedData });
  };

  onSearchTextChanged = ({ target: { value } }) => {
    this.props.setSearchInputValue({ value });
  };

  getSortedIdsByDate = () => {
    const formattedDates = this.props.data.map((item) => {
      const date = dateUtils.getDateByString(item.date);
      return { id: item.id, date: date.getTime() };
    });

    return _.sortBy(formattedDates, 'date').map(item => item.id);
  };

  render() {
    const {
      selectedField, selectedSortType, searchInputValue, sortedData,
    } = this.props;

    return (
      <Content>
        <CustomSelect
          isEmptyOption
          value={selectedField}
          options={dataObjectKeys}
          onOptionClick={this.onFieldOptionClick}
        />
        <CustomSelect
          isDisabled={!selectedField.value}
          value={selectedSortType}
          options={sortSelectOptions}
          onOptionClick={this.onSortTypeOptionClick}
        />
        <SearchInput
          value={searchInputValue}
          onChangeText={this.onSearchTextChanged}
          disabled={!selectedField.value}
        />
        <Table headerCellItems={dataObjectKeys}>
          {sortedData
            .filter((item) => {
              if (!selectedField.value) {
                return true;
              }
              return _.toLower(item[selectedField.value]).indexOf(searchInputValue) > -1;
            })
            .map(item => (
              <Row key={item.id} item={item} activeFieldKey={selectedField.value} />
            ))}
        </Table>
      </Content>
    );
  }
}

export default MainPageComponent;
