import { createAction } from 'redux-actions';

export const setSelectedField = createAction('SET_SELECTED_FIELD');
export const setSelectedSortType = createAction('SET_SELECTED_S0RT_TYPE');
export const setSortedData = createAction('SET_SORTED_DATA');
export const setSearchInputValue = createAction('SET_SEARCH_INPUT_VALUE');
