export const dataObjectKeys = [
  {
    value: 'name',
    label: 'Имя',
  },
  {
    value: 'date',
    label: 'Дата',
  },
  {
    value: 'count',
    label: 'Количество',
  },
];
