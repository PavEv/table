export const ascSortKey = 'asc';
export const descSortKey = 'desc';

export const sortSelectOptions = [
  { value: '', label: 'Без сортировки' },
  { value: ascSortKey, label: 'По возрастанию' },
  { value: descSortKey, label: 'По убыванию' },
];

export const emptySelectOption = { value: '', label: 'Не выбрано' };
