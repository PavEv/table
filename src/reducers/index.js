import { combineReducers } from 'redux';

import mainPage from '../containers/MainPage/reducer';

const rootReducer = () => combineReducers({
  mainPage,
});

export default rootReducer;
